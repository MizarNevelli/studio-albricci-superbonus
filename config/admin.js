module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '49a5a4ba151eb7b91aa4238c9d41f882'),
  },
});
