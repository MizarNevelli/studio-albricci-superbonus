import MyLogo from './extensions/studio-albricci.jpeg';

export default {
  config: {
    locales: [
      'it',
    ],
    auth: {
      logo: MyLogo
    },
    menu: {
      logo: MyLogo
    },
    // head: {
    //   favicon: Favicon,
    // },
    tutorials: false,
    notifications: {
      release: false,
    }
  },
  bootstrap() { },
};
